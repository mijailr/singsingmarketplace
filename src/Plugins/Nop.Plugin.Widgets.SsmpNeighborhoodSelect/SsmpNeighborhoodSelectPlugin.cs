﻿using System.Collections.Generic;
using Nop.Core;
using Nop.Core.Infrastructure;
using Nop.Services.Cms;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.Plugins;
using Nop.Web.Framework.Infrastructure;

namespace Nop.Plugin.Widgets.SsmpNeighborhoodSelect
{
    /// <summary>
    /// PLugin
    /// </summary>
    public class SsmpNeighborhoodSelectPlugin : BasePlugin, IWidgetPlugin
    {
        private readonly ILocalizationService _localizationService;
        private readonly ISettingService _settingService;
        private readonly IWebHelper _webHelper;
        private readonly INopFileProvider _fileProvider;

        public SsmpNeighborhoodSelectPlugin(ILocalizationService localizationService,
            IPictureService pictureService,
            ISettingService settingService,
            IWebHelper webHelper,
            INopFileProvider fileProvider)
        {
            _localizationService = localizationService;
            _settingService = settingService;
            _webHelper = webHelper;
            _fileProvider = fileProvider;
        }

        #region BasePlugin Overrides
        /// <summary>
        /// Gets a configuration page URL to attach as a clickable link to the "Configure" button 
        /// that shows in the backend plugin grid next to the plugin. Note that 
        /// _webHelper.GetStoreLocation() returns the store's base URL such as
        /// http://dev.singsingmarketplace.com/". When the user clicks the "Configure" button, a
        /// call is made to the WidgetsSsmpNeighborhoodSelectController:Configure action. The
        /// "Admin/SsmpNeighborhoodSelect/Configure" is defined in the infrastructure folder in the
        /// RouteProvider class which implements IRouteProvider to override the nop default routing.
        /// </summary>
        public override string GetConfigurationPageUrl()
        {
            return _webHelper.GetStoreLocation() + Helper.Constants.NeighborhoodSelectConfigureRouteValue;
        }

        /// <summary>
        /// Install plugin
        /// </summary>
        public override void Install()
        {
            // insert our Google Place API key
            var settings = new SsmpNeighborhoodSelectSettings
            {
                GooglePlacesApiKey = Helper.Constants.ApiKeys.DefaultNeighborhoodPluginGooglePlacesApiKey
            };
            _settingService.SaveSetting(settings);

            ////pictures
            //var sampleImagesPath = _fileProvider.MapPath("~/Plugins/Widgets.NivoSlider/Content/nivoslider/sample-images/");

            ////settings
            //var settings = new SsmpNeighborhoodSelectSettings
            //{
            //    Picture1Id = _pictureService.InsertPicture(_fileProvider.ReadAllBytes(_fileProvider.Combine(sampleImagesPath, "banner1.jpg")), MimeTypes.ImagePJpeg, "banner_1").Id,
            //    Text1 = "",
            //    Link1 = _webHelper.GetStoreLocation(false),
            //    Picture2Id = _pictureService.InsertPicture(_fileProvider.ReadAllBytes(_fileProvider.Combine(sampleImagesPath, "banner2.jpg")), MimeTypes.ImagePJpeg, "banner_2").Id,
            //    Text2 = "",
            //    Link2 = _webHelper.GetStoreLocation(false)
            //    //Picture3Id = _pictureService.InsertPicture(File.ReadAllBytes(_fileProvider.Combine(sampleImagesPath,"banner3.jpg")), MimeTypes.ImagePJpeg, "banner_3").Id,
            //    //Text3 = "",
            //    //Link3 = _webHelper.GetStoreLocation(false),
            //};
            //_settingService.SaveSetting(settings);

            //_localizationService.AddPluginLocaleResource(new Dictionary<string, string>
            //{
            //    ["Plugins.Widgets.NivoSlider.Picture1"] = "Picture 1",
            //    ["Plugins.Widgets.NivoSlider.Picture2"] = "Picture 2",
            //    ["Plugins.Widgets.NivoSlider.Picture3"] = "Picture 3",
            //    ["Plugins.Widgets.NivoSlider.Picture4"] = "Picture 4",
            //    ["Plugins.Widgets.NivoSlider.Picture5"] = "Picture 5",
            //    ["Plugins.Widgets.NivoSlider.Picture"] = "Picture",
            //    ["Plugins.Widgets.NivoSlider.Picture.Hint"] = "Upload picture.",
            //    ["Plugins.Widgets.NivoSlider.Text"] = "Comment",
            //    ["Plugins.Widgets.NivoSlider.Text.Hint"] = "Enter comment for picture. Leave empty if you don't want to display any text.",
            //    ["Plugins.Widgets.NivoSlider.Link"] = "URL",
            //    ["Plugins.Widgets.NivoSlider.Link.Hint"] = "Enter URL. Leave empty if you don't want this picture to be clickable.",
            //    ["Plugins.Widgets.NivoSlider.AltText"] = "Image alternate text",
            //    ["Plugins.Widgets.NivoSlider.AltText.Hint"] = "Enter alternate text that will be added to image."
            //});

            base.Install();
        }

        /// <summary>
        /// Uninstall plugin
        /// </summary>
        public override void Uninstall()
        {
            // remove our Google Place API key
            _settingService.DeleteSetting<SsmpNeighborhoodSelectSettings>();

            ////settings
            //_settingService.DeleteSetting<NivoSliderSettings>();

            ////locales
            //_localizationService.DeletePluginLocaleResources("Plugins.Widgets.NivoSlider");

            base.Uninstall();
        }
        #endregion BasePlugin Overrides

        #region IWidgetPlugin Implementation
        /// <summary>
        /// Gets widget zones where this widget should be rendered
        /// </summary>
        /// <returns>Widget zones</returns>
        public IList<string> GetWidgetZones()
        {
            // Define where on the front-end we want to place our neighborhood drop down
            return new List<string> { PublicWidgetZones.SearchBoxBeforeSearchButton };
        }

        /// <summary>
        /// Gets a value indicating whether to hide this plugin on the widget list page in the admin area
        /// </summary>
        public bool HideInWidgetList => false;

        /// <summary>
        /// Gets a name of a view component for displaying widget
        /// </summary>
        /// <param name="widgetZone">Name of the widget zone</param>
        /// <returns>View component name</returns>
        public string GetWidgetViewComponentName(string widgetZone)
        {
            return "WidgetsSsmpNeighborhoodSelect";
        }
        #endregion IWidgetPlugin Implementation
    }
}