﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Widgets.SsmpNeighborhoodSelect.Helper
{
    public static class Constants
    {
        /// <summary>
        /// table names
        /// </summary>
        public struct TableNames
        {
            public const string Neighborhood = "SSMP_Neighborhood";
            public const string ProductInNeighborhood = "SSMP_ProductInNeighborhood";
            public const string SingSingNeighborhood = "SSMP_SingSingNeighborhood";
            public const string DeliveryOption = "SSMP_DeliveryOption";
            public const string ProductToDeliveryOption = "SSMP_ProductToDeliveryOption";
            public const string NegotiatedPrice = "SSMP_NegotiatedPrice";
            public const string NegotiatedPriceHistory = "SSMP_NegotiatedPriceHistory";
            public const string NegotiatedOrder = "SSMP_NegotiatedOrder";
            public const string NegotiatedOrderItem = "SSMP_NegotiatedOrderItem";
            public const string SSMP_Vendor = "SSMP_Vendor";
        }

        /// <summary>
        /// API keys
        /// </summary>
        public struct ApiKeys
        {
            public const string DefaultNeighborhoodPluginGooglePlacesApiKey = "AIzaSyBPQsx36MH74ovjCp5PR833ynGKRw02yqY";
        }

        /// <summary>
        /// defaults
        /// </summary>
        public struct Defaults
        {
            public const string DefaultIsoCountryCode = "NG";
        }

        /// <summary>
        /// language strings
        /// </summary>
        public struct Locales
        {
            // setting applicatable to all neighborhoods
            public struct Settings
            {
                public const string GooglePlacesApiKeyLabelName = "Plugins.Widgets.Neighborhood.GooglePlacesApiKey";
                public const string GooglePlacesApiKeyHintName = "Plugins.Widgets.Neighborhood.GooglePlacesApiKey.Hint"; // the ? mark
                public const string GooglePlacesApiKeyLabelValue = "Enter the Google Places API Key.";
                public const string GooglePlacesApiKeyHintValue = "Enter the Google Places API Key."; // the ? mark

                public const string NeighborhoodPluginInstructionsName = "Plugins.Widgets.Neighborhood.Instructions";
                public const string NeighborhoodPluginInstructionsValue =
                    "<p>SingSing Marketplace Neighborhood plugin enable us to pull down neighborhoods from around the world."
                    + "<br />We can can then attach products to these neighborhoods so that they will be filtered on the front store."
                    + "<br />Because we are using Google's Places API to retrieve these neighborhoods, you will need to enter the API Key below:"
                    + "<br /><ul><li><a href=\"https://developers.google.com/places/web-service/get-api-key\""
                    + "target=\"_blank\">If you don't already have a key, click the link</a> and follow the wizard.</li>"
                    + "<li>Enter the key into the 'Google Places API Key' box below</li>"
                    + "<li>Click the 'Save' button below and key will be available to for use throughout the store</li></ul></p>";
                public const string NeighborhoodPluginProductsInNeighborhoodTitleName = "Plugins.Widgets.Neighborhood.ProductsInNeighborhoodTitle";
                public const string NeighborhoodPluginProductsInNeighborhoodTitleValue = "Attach Products to Neighborhoods";
            }

            //neighborhood table fields
            public struct Fields
            {

                public const string SingSingNeighborhoodLabelName = "Plugins.Widgets.Neighborhood.Fields.SingSingNeighborhood";
                public const string SingSingNeighborhoodLabelValue = "SINGSING Neighborhood";
                public const string SingSingNeighborhoodHintName = "Plugins.Widgets.Neighborhood.Fields.SingSingNeighborhood.Hint";
                public const string SingSingNeighborhoodHintValue = "The well-known SINGSING Neighborhood this location maps to";


                public const string NeighborhoodSearchStringLabelName = "Plugins.Widgets.Neighborhood.Fields.NeighborhoodSearchString";
                public const string NeighborhoodSearchStringLabelValue = "Google Search String";
                public const string NeighborhoodSearchStringHintName = "Plugins.Widgets.Neighborhood.Fields.NeighborhoodSearchString.Hint";
                public const string NeighborhoodSearchStringHintValue = "The original string entered in the Google search box";

                public const string PlaceIdLabelName = "Plugins.Widgets.Neighborhood.Fields.PlaceId";
                public const string PlaceIdLabelValue = "Place ID";
                public const string PlaceIdHintName = "Plugins.Widgets.Neighborhood.Fields.PlaceId.Hint";
                public const string PlaceIdHintValue = "The Place Id from the Google Places API";

                public const string RouteLabelName = "Plugins.Widgets.Neighborhood.Fields.Route";
                public const string RouteLabelValue = "Route";
                public const string RouteHintName = "Plugins.Widgets.Neighborhood.Fields.Route.Hint";
                public const string RouteHintValue = "The Route from the Google Places API";

                public const string NeighborhoodLabelName = "Plugins.Widgets.Neighborhood.Fields.Neighborhood";
                public const string NeighborhoodLabelValue = "Neighborhood";
                public const string NeighborhoodHintName = "Plugins.Widgets.Neighborhood.Fields.Neighborhood.Hint";
                public const string NeighborhoodHintValue = "Neighborhood";

                public const string NaturalFeatureLabelName = "Plugins.Widgets.Neighborhood.Fields.NaturalFeature";
                public const string NaturalFeatureLabelValue = "Natural Feature";
                public const string NaturalFeatureHintName = "Plugins.Widgets.Neighborhood.Fields.NaturalFeature.Hint";
                public const string NaturalFeatureHintValue = "Natural Feature";

                public const string AdministrativeAreaLevel3LabelName = "Plugins.Widgets.Neighborhood.Fields.AdministrativeAreaLevel3";
                public const string AdministrativeAreaLevel3LabelValue = "Administrative Area Level3";
                public const string AdministrativeAreaLevel3HintName = "Plugins.Widgets.Neighborhood.Fields.AdministrativeAreaLevel3.Hint";
                public const string AdministrativeAreaLevel3HintValue = "Administrative Area Level3";

                public const string AdministrativeAreaLevel2LabelName = "Plugins.Widgets.Neighborhood.Fields.AdministrativeAreaLevel2";
                public const string AdministrativeAreaLevel2LabelValue = "Administrative Area Level2";
                public const string AdministrativeAreaLevel2HintName = "Plugins.Widgets.Neighborhood.Fields.AdministrativeAreaLevel2.Hint";
                public const string AdministrativeAreaLevel2HintValue = "Administrative Area Level2";

                public const string AdministrativeAreaLevel1LabelName = "Plugins.Widgets.Neighborhood.Fields.AdministrativeAreaLevel1";
                public const string AdministrativeAreaLevel1LabelValue = "Administrative Area Level1";
                public const string AdministrativeAreaLevel1HintName = "Plugins.Widgets.Neighborhood.Fields.AdministrativeAreaLevel1.Hint";
                public const string AdministrativeAreaLevel1HintValue = "Administrative Area Level1";

                public const string LocalityLabelName = "Plugins.Widgets.Neighborhood.Fields.Locality";
                public const string LocalityLabelValue = "Locality";
                public const string LocalityHintName = "Plugins.Widgets.Neighborhood.Fields.Locality.Hint";
                public const string LocalityHintValue = "Locality";

                public const string SubLocalityLevel1LabelName = "Plugins.Widgets.Neighborhood.Fields.SubLocalityLevel1";
                public const string SubLocalityLevel1LabelValue = "Sub Locality Level1";
                public const string SubLocalityLevel1HintName = "Plugins.Widgets.Neighborhood.Fields.SubLocalityLevel1.Hint";
                public const string SubLocalityLevel1HintValue = "Sub Locality Level1";

                public const string CountryLabelName = "Plugins.Widgets.Neighborhood.Fields.Country";
                public const string CountryLabelValue = "Country";
                public const string CountryHintName = "Plugins.Widgets.Neighborhood.Fields.Country.Hint";
                public const string CountryHintValue = "Country";

                public const string TwoLetterIsoCodeLabelName = "Plugins.Widgets.Neighborhood.Fields.TwoLetterIsoCode";
                public const string TwoLetterIsoCodeLabelValue = "Two Letter ISO Code";
                public const string TwoLetterIsoCodeHintName = "Plugins.Widgets.Neighborhood.Fields.TwoLetterIsoCode.Hint";
                public const string TwoLetterIsoCodeHintValue = "Two Letter ISO Code";

                public const string DisplayOrderLabelName = "Plugins.Widgets.Neighborhood.Fields.DisplayOrder";
                public const string DisplayOrderLabelValue = "Display Order";
                public const string DisplayOrderHintName = "Plugins.Widgets.Neighborhood.Fields.DisplayOrder.Hint";
                public const string DisplayOrderHintValue = "Display Order";

                public const string DeliveryOptionLabelName = "Plugins.Widgets.Neighborhood.Fields.DeliveryOption";
                public const string DeliveryOptionLabelValue = "Delivery Option";
                public const string DeliveryOptionHintName = "Plugins.Widgets.Neighborhood.Fields.DeliveryOption.Hint";
                public const string DeliveryOptionHintValue = "Delivery Option";




            }

            public struct Labels
            {
                public const string NeighborhoodsLabelName = "Plugins.Widgets.Neighborhood.Labels.Neighborhoods";
                public const string NeighborhoodsLabelValue = "Neighborhoods";

                public const string GooglePlacesLabelName = "Plugins.Widgets.Neighborhood.Labels.GooglePlaces";
                public const string GooglePlacesLabelValue = "Google Places";

                public const string ProductAndTheirNeighborhoodsLabelName = "Plugins.Widgets.Neighborhood.Labels.ProductAndTheirNeighborhoods";
                public const string ProductAndTheirNeighborhoodsLabelValue = "Products in Neighborhoods";

                public const string NeighborhoodAddNewLabelName = "Plugins.Widgets.Neighborhood.Labels.AddNew";
                public const string NeighborhoodAddNewLabelValue = "Add a new neighborhood";

                public const string SearchByLocationLableName = "Plugins.Widgets.Neighborhood.Labels.SearchByNeighborhood";
                public const string SearchByLocationLableValue = "Search location";
                public const string SearchByLocationHintName = "Plugins.Widgets.Neighborhood.Labels.SearchByNeighborhood.Hint";
                public const string SearchByLocationHintValue = "Search location";
                public const string SearchByNeighborhoodTooltipName = "Plugins.Widgets.Neighborhood.Labels.SearchByNeighborhood.Tooltip";
                public const string SearchByNeighborhoodTooltipValue = "Enter Shopping Neighborhood";
                public const string ProductDeliveryOptionLableName = "Plugins.Widgets.Neighborhood.Labels.ProductDelverlyOption";
                public const string ProductDeliveryOptionLableValue = "Delivery Option";
            }

            public struct Misc
            {
                public const string NeighborhoodNotMapped = "Neighborhood Not Mapped";
                public const string NeighborhoodNotMappedLabelName = "Plugins.Widgets.Neighborhood.Labels.NeighborhoodNotMapped";
                public const string NeighborhoodNotMappedLabelValue = "Neighborhood Not Mapped";
                public const string DefaultMappedSingSingNeighborhoodId = "1";
                public const string NeighborhoodNoneRecord = "None";
            }

            public struct StatusMessages
            {
                public const string NeighborhoodSavedSuccessfullyLabelName = "Plugins.Widgets.Neighborhood.Status.SavedSuccessfully";
                public const string NeighborhoodSavedSuccessfullyLabelValue = "Neighborhood Saved Successfully";
                public const string NeighborhoodDuplicateFoundLabelName = "Plugins.Widgets.Neighborhood.Status.NeighborhoodDuplicateFound";
                public const string NeighborhoodDuplicateFoundLabelLabelValue = "Neighborhood Already Exist";
                public const string NeighborhoodEnterLocationLabelName = "Plugins.Widgets.Neighborhood.SearchBylocation.EnterLocation";
                public const string NeighborhoodEnterLocationLabelValue = "Please Enter Location";
            }

            public struct CheckoutMessage
            {
                public const string ProductExistWithSameNeighbourhoodName = "Cart.ProductExistWithSameNeighbourhood.Error";
                public const string ProductExistWithSameNeighbourhoodValue = "You already have items from other neighborhoods in your shopping cart. Complete that neighborhood transaction before starting a new one. Or remove those items from the shopping before beginning with another neighborhood";
                public const string ProductExistWithSameDeliveryOptionName = "Cart.ProductExistWithSameDeliveryOption.Error";
                public const string ProductExistWithSameDeliveryOptionValue = "You already have items marked for delivery type '{0}' in your shopping cart. Complete that delivery type transaction before starting a new one. Or remove those items from the shopping before beginning with another delivery type";
                public const string WrongNeighbourhoodName = "Checkout.WrongNeighbourhood.Error";
                public const string WrongNeighbourhoodValue = "Delivery neighborhood must match the current shopping neighborhood of '{0}'";
            }

        }



        #region routes
        /// <summary>
        /// Gets a name of the route to the import contacts callback
        /// </summary>
        public static string NeighborhoodSelectRouteName => "Plugin.Widgets.SsmpNeighborhoodSelect.Search";

        /// <summary>
        /// Gets the name of the route to configure the SsmpNeighborhoodSelect plugin
        /// </summary>
        public static string NeighborhoodSelectConfigureRouteName => "Plugin.Widgets.SsmpNeighborhoodSelect.Configure";

        /// <summary>
        /// Gets the value of the route to configure the SsmpNeighborhoodSelect plugin
        /// </summary>
        public static string NeighborhoodSelectConfigureRouteValue => "Admin/SsmpNeighborhoodSelect/Configure";
        #endregion routes
    }
}

