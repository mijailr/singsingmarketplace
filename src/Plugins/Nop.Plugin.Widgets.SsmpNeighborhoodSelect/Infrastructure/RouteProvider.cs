﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc.Routing;

namespace Nop.Plugin.Widgets.SsmpNeighborhoodSelect.Infrastructure
{
    /// <summary>
    /// Represents plugin route provider
    /// </summary>
    public class RouteProvider : IRouteProvider
    {
        /// <summary>
        /// Register routes
        /// </summary>
        /// <param name="endpointRouteBuilder">Route builder</param>
        public void RegisterRoutes(IEndpointRouteBuilder endpointRouteBuilder)
        {
            // Add the Configuration Route
            endpointRouteBuilder.MapControllerRoute(Helper.Constants.NeighborhoodSelectConfigureRouteName, Helper.Constants.NeighborhoodSelectConfigureRouteValue,
                new { controller = "WidgetsSsmpNeighborhoodSelect", 
                    action = "Configure", 
                    area = AreaNames.Admin });
        }

        /// <summary>
        /// Gets a priority of route provider
        /// </summary>
        public int Priority => 1; //set a value that is greater than the default one in Nop.Web to override routes
    }
}