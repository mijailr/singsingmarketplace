﻿using System;
using System.Collections.Generic;
using System.Text;
using Nop.Web.Framework.Models;

namespace Nop.Plugin.Widgets.SsmpNeighborhoodSelect.Models
{
    public class PublicInfoModel : BaseNopModel
    {
        public PublicInfoModel()
        {

        }

        public string Warning { get; set; }

        public bool NoResults { get; set; }

        /// <summary>
        /// place id as defined by Google Places
        /// </summary>
        public string Pid { get; set; }

        /// <summary>
        /// Google places api key for manage google api
        /// </summary>
        public string GooglePlacesApiKey { get; set; }
    }
}
