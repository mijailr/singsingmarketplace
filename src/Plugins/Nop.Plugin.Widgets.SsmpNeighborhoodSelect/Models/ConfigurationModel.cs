﻿using System.ComponentModel.DataAnnotations;
using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Models;

namespace Nop.Plugin.Widgets.SsmpNeighborhoodSelect.Models
{
    public class ConfigurationModel : BaseNopModel
    {
        public int ActiveStoreScopeConfiguration { get; set; }

        [NopResourceDisplayName(Helper.Constants.Locales.Settings.GooglePlacesApiKeyLabelName)]
        public string GooglePlacesApiKey { get; set; }

        public bool GooglePlacesApiKey_OverrideForStore { get; set; }
    }
}