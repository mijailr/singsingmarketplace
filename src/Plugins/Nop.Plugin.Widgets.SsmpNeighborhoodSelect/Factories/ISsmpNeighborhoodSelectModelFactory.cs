﻿using Nop.Plugin.Widgets.SsmpNeighborhoodSelect.Models;

namespace Nop.Plugin.Widgets.SsmpNeighborhoodSelect.Factories
{
    public interface ISsmpNeighborhoodSelectModelFactory
    {
        PublicInfoModel PreparePublicInfoModel();
    }
}
