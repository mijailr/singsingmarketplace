﻿using System;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Plugin.Widgets.SsmpNeighborhoodSelect.Factories;
using Nop.Plugin.Widgets.SsmpNeighborhoodSelect.Infrastructure.Cache;
using Nop.Plugin.Widgets.SsmpNeighborhoodSelect.Models;
using Nop.Services.Caching;
using Nop.Services.Configuration;
using Nop.Services.Media;
using Nop.Web.Framework.Components;

namespace Nop.Plugin.Widgets.SsmpNeighborhoodSelect.Components
{
    [ViewComponent(Name = "WidgetsSsmpNeighborhoodSelect")]
    public class WidgetsSsmpNeighborhoodSelectViewComponent : NopViewComponent
    {
        private readonly ICacheKeyService _cacheKeyService;
        private readonly IStoreContext _storeContext;
        private readonly IStaticCacheManager _staticCacheManager;
        private readonly ISettingService _settingService;
        private readonly IPictureService _pictureService;
        private readonly IWebHelper _webHelper;
        private readonly ISsmpNeighborhoodSelectModelFactory _ssmpNeighborhoodSelectModelFactory;

        public WidgetsSsmpNeighborhoodSelectViewComponent(ICacheKeyService cacheKeyService,
            IStoreContext storeContext, 
            IStaticCacheManager staticCacheManager, 
            ISettingService settingService, 
            IPictureService pictureService,
            IWebHelper webHelper,
            ISsmpNeighborhoodSelectModelFactory ssmpNeighborhoodSelectModelFactory)
        {
            _cacheKeyService = cacheKeyService;
            _storeContext = storeContext;
            _staticCacheManager = staticCacheManager;
            _settingService = settingService;
            _pictureService = pictureService;
            _webHelper = webHelper;
            _ssmpNeighborhoodSelectModelFactory = ssmpNeighborhoodSelectModelFactory;

        }

        public IViewComponentResult Invoke(string widgetZone, object additionalData)
        {
            var model = _ssmpNeighborhoodSelectModelFactory.PreparePublicInfoModel();
            return View("~/Plugins/Widgets.SsmpNeighborhoodSelect/Views/PublicInfo.cshtml", model);
        }
    }
}
